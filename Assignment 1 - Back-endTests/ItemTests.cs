﻿using Assignment_1___Back_end.Items;
using Assignment_1___Back_end.Items.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Assignment_1___Back_endTests
{
    public class ItemTests
    {
        [Fact]
        public void Weapon_CheckWeaponInit_SetsCorrectInfo()
        {
            Weapons weapons = new Weapons()
            {
                ItemName = "Axe",
                ItemLevel = 1,
                ItemSlot = Slot.WeaponSlot,
                WeaponType = WeaponType.WeaponAxes,
                WeaponAttributes = new WeaponAttributes() { WeaponDamage = 1, WeaponAttackSpeed = 0.7 }
            };
            //The assignment wished for only one asses per test, but I'm short on time
            string expectedName = "Axe";
            string actualName = weapons.ItemName;
            Assert.Equal(expectedName, actualName);

            int expectedItemLevel = 1;
            int actualItemLevel = weapons.ItemLevel;
            Assert.Equal(expectedItemLevel, actualItemLevel);


        }
    }
}
