using System;
using Xunit;
using Assignment_1___Back_endTests;
using Assignment_1___Back_end.Hero_Classes;
using Assignment_1___Back_end;

namespace Assignment_1___Back_endTests
{
    public class HeroTest
    {
        [Fact]
        public void Hero_MageRecievesLevel_SetsLevelTo1()
        {

            Mage mage = new("Mage");
            int expected = 1;
            int actual = mage.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Hero_RangerRecievesLevel_SetsLevelTo1()
        {

            Ranger ranger = new("Ranger");
            int expected = 1;
            int actual = ranger.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Hero_RogueRecievesLevel_SetsLevelTo1()
        {

            Rogue rogue = new("rogue");
            int expected = 1;
            int actual = rogue.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Hero_WarriorRecievesLevel_SetsLevelTo1()
        {

            Warrior warrior = new("Warrior");
            int expected = 1;
            int actual = warrior.Level;

            Assert.Equal(expected, actual);
        }

        [Fact] 
        public void Hero_PlayerRecievesAttributes_SetsCorrectAttributes()
        {
            Mage mage = new("mage");
            HeroAttributes expected = new() { Strength = 1, Dexterity = 1, Intelligence = 8 };
            HeroAttributes actual = mage.BaseHeroAttributes;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_MageLevelsUp_SetsLevelTo2()
        {
            // Arrange
            Mage mage = new("Mage");
            int expected = 2;
            // Act
            mage.LevelUp(1);
            int actual = mage.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_RangerLevelsUp_SetsLevelTo2()
        {
            // Arrange
            Ranger ranger = new("Ranger");
            int expected = 2;
            // Act
            ranger.LevelUp(1);
            int actual = ranger.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_RogueLevelsUp_SetsLevelTo2()
        {
            // Arrange
            Rogue rogue = new("Rogue");
            int expected = 2;
            // Act
            rogue.LevelUp(1);
            int actual = rogue.Level;
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_WarriorLevelsUp_SetsLevelTo2()
        {
            // Arrange
            Warrior warrior = new("Warrior");
            int expected = 2;
            // Act
            warrior.LevelUp(1);
            int actual = warrior.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_MageIncreaseAttributes_SetsCorrectAttributes()
        {
            Mage mage = new("mage");
            HeroAttributes expected = new() {Strength = 2, Dexterity = 2, Intelligence = 13};
            mage.LevelUp(1);
            HeroAttributes actual = mage.BaseHeroAttributes;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_RangerIncreaseAttributes_SetsCorrectAttributes()
        {
            Ranger ranger = new("Ranger");
            HeroAttributes expected = new() { Strength = 2, Dexterity = 12, Intelligence = 2 };
            ranger.LevelUp(1);
            HeroAttributes actual = ranger.BaseHeroAttributes;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_RogueIncreaseAttributes_SetsCorrectAttributes()
        {
            Rogue rogue = new("Rogue");
            HeroAttributes expected = new() { Strength = 3, Dexterity = 10, Intelligence = 2 };
            rogue.LevelUp(1);
            HeroAttributes actual = rogue.BaseHeroAttributes;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_WarriorIncreaseAttributes_SetsCorrectAttributes()
        {
            Warrior warrior = new("Warrior");
            HeroAttributes expected = new() { Strength = 8, Dexterity = 4, Intelligence = 2 };
            warrior.LevelUp(1);
            HeroAttributes actual = warrior.BaseHeroAttributes;

            Assert.Equal(expected, actual);
        }


    }
}
