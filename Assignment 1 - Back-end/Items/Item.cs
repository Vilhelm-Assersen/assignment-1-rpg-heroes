﻿using System;
using System.Collections.Generic;

namespace Assignment_1___Back_end.Items
{
    public abstract class Item
    {
        public Slot ItemSlot { get; set; }
        public string ItemName { get; set; }
        public int ItemLevel { get; set; }
        //public abstract string ItemDescription(); //TODO: Add item description to all items
    }
    public enum Slot
    {
        WeaponSlot,
        HeadSlot,
        BodySlot,
        LegSlot
    }
}
