﻿using System;
using System.Collections.Generic;

namespace Assignment_1___Back_end.Items.Armor
{
    public enum ArmorType
    {
        ArmorCloth,
        ArmorLeather,
        ArmorMail,
        ArmorPlate
    }
    public class Armor : Item
    {
        public ArmorType ArmorType { get; set; }
        public HeroAttributes Attributes { get; set; }
        //public int ItemLevel { get; set; }
    }
}
