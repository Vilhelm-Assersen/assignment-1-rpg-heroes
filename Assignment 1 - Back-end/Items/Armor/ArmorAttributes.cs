﻿namespace Assignment_1___Back_end.Items.Armor
{
    public class ArmorAttributes
    {
        public ArmorType ArmorType { get; set; }
        public HeroAttributes Attributes { get; set; }
    }
}