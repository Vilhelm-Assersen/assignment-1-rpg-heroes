﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1___Back_end.Items.Custom_Item_Exceptions
{
    /// <summary>
    /// An exception for when a player tries to equip an invalid weapon
    /// </summary>
    internal class InvalidWeaponException : Exception
    {
        public InvalidWeaponException() 
        { 
        }

        public InvalidWeaponException(string message) : base(message) 
        { 
        }

        public override string Message => "Invalid weapon exception";
    }
}
