﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1___Back_end.Items.Custom_Item_Exceptions
{
    /// <summary>
    /// An exception for when a player tries to equip an invalid armor
    /// </summary>
    internal class InvalidArmorException : Exception
    {
        public InvalidArmorException() { }
        public InvalidArmorException(string message) : base(message)
        {
        }
        public override string Message => "Invalid armor exception";
    }
}
