﻿using System;
using System.Collections.Generic;

namespace Assignment_1___Back_end.Items.Weapons
{
    public enum WeaponType
    {
        WeaponAxes,
        WeaponBows,
        WeaponDaggers,
        WeaponHammers,
        WeaponStaffs,
        WeaponSwords,
        WeaponWands
    }
    
    public class Weapons : Item
    {
        public WeaponType WeaponType { get; set; }
        public WeaponAttributes WeaponAttributes { get; set; }
        public int ItemLevel { get; set; }
        //TODO Create too high level exception
    }
}
