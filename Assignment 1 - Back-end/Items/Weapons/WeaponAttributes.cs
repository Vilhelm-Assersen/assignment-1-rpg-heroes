﻿namespace Assignment_1___Back_end.Items.Weapons
{
    public class WeaponAttributes
    {
        public int WeaponDamage {get; set; }
        public double WeaponAttackSpeed { get; set; }
    }
}