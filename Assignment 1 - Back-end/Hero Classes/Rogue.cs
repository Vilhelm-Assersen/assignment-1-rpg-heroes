﻿using Assignment_1___Back_end.Items.Armor;
using Assignment_1___Back_end.Items.Custom_Item_Exceptions;
using Assignment_1___Back_end.Items.Weapons;
using System;

namespace Assignment_1___Back_end.Hero_Classes
{
    public class Rogue : Hero
    {
        public Rogue(string name) : base(name, 2, 6, 1) //Figure out how "base" works
        {
        }
        /// <summary>
        /// LevelUp() overrides the virtual LevelUp() function in order to give the rogue it's unique stat increase according to how many levels they gained
        /// The players level is also increaset according to how many levels the player gained this instance
        /// </summary>
        /// <param name="levels"></param>
        public override void LevelUp(int levels)
        {
            HeroAttributes levelUpAttribute = new() { Strength = 1 * levels, Dexterity = 4 * levels, Intelligence = 1 * levels };
            BaseHeroAttributes.Strength += levelUpAttribute.Strength;
            BaseHeroAttributes.Dexterity += levelUpAttribute.Dexterity;
            BaseHeroAttributes.Intelligence += levelUpAttribute.Intelligence;
            Level += levels;
        }
        public override string Equip(Weapons weapon)
        {
            if (weapon.ItemLevel > Level)
            {
                throw new InvalidWeaponException($"You need to be at least level {weapon.ItemLevel} to equip the {weapon.WeaponType}");
            }
            if (weapon.WeaponType != WeaponType.WeaponDaggers && weapon.WeaponType != WeaponType.WeaponSwords)
            {
                throw new InvalidWeaponException($"The mage class is only able to equip weapons: {WeaponType.WeaponStaffs} and {WeaponType.WeaponWands}");
            }
            return $"You have now equiped the {weapon.WeaponType}";
        }
        public override string Equip(Armor armor)
        {
            if (armor.ItemLevel > Level)
            {
                throw new InvalidArmorException($"You need to be at least level {armor.ItemLevel} to equip the {armor.ArmorType}");
            }
            if (armor.ArmorType != ArmorType.ArmorLeather && armor.ArmorType != ArmorType.ArmorMail)
            {
                throw new InvalidArmorException($"The mage class is only able to equip armors: {ArmorType.ArmorCloth}");
            }
            return $"You have now equiped the {armor.ArmorType}";
        }
        public override double CalculateDamage()
        {
            TotalHeroAttributes = CalculateArmorBonus();
            double weaponDamage = CalculateDamage();
            if (weaponDamage == 1)
            {
                return 1;
            }
            double multiplier = 1 + TotalHeroAttributes.Dexterity / 100;
            return multiplier * weaponDamage;
        }
    }
}
