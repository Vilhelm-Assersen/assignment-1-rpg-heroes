﻿using Assignment_1___Back_end.Hero_Classes;
using System.Collections.Generic;
using System;
using Assignment_1___Back_end.Items;
using Assignment_1___Back_end.Items.Armor;
using Assignment_1___Back_end.Items.Weapons;
using System.Xml.Schema;
using System.ComponentModel;
using System.Xml;

namespace Assignment_1___Back_end
{
    public abstract class Hero
    {

        public string Name { get; set; }  
        public int Level { get; set; }  
        public HeroAttributes BaseHeroAttributes { get; set; }
        public HeroAttributes TotalHeroAttributes { get; set; }
        public Dictionary<Slot, Item> Equipment { get; set; } // Had to create "Items.cs" first
        public double Damage { get; set; }


        public Hero(string name, int strength, int dexterity, int intelligence)
        {
            Name = name;
            Level = 1;
            Equipment = new Dictionary<Slot, Item>();
            BaseHeroAttributes = new HeroAttributes() { Strength = strength, Dexterity = dexterity, Intelligence = intelligence};
        }

        //Levels up the player and gives the respective additional attributes
        public abstract void LevelUp(int levels);

        //Allows the players to equip weapons. If the player tries to equip an illegal weapon. An exception is returnet instead
        public abstract string Equip(Weapons weapon);
        
        //Allows the players to equip armor. If the player tries to equip an illegal armor. An exception is returnet instead
        public abstract string Equip(Armor armor);

        //
        public void CalculateTotalAttributes()
        {
            TotalHeroAttributes = CalculateArmorBonus();
            Damage = CalculateDamage();
        }
        public void DisplayStats()
        { 
            CalculateTotalAttributes();
            Console.WriteLine($"Player name: {Name}, Level: {Level}, Player attributes: {TotalHeroAttributes}, Player damage: {Damage}");
        }

        /*public void CalculateTotalStats()
        {

        }*/

        //CalculateDamage() allows the player to see how much damage they deal depending on what class the player is playing
        public abstract double CalculateDamage();

        //CalculateDamage() allows the player to see how much damage they deal depending on what class the player is playing
        public HeroAttributes CalculateArmorBonus() 
        {
            HeroAttributes armorBonus = new() { Strength = 0, Dexterity = 0, Intelligence = 0 };

            bool checkHeadArmor = Equipment.TryGetValue(Slot.HeadSlot, out Item headArmor);
            bool checkBodyArmor = Equipment.TryGetValue(Slot.BodySlot, out Item bodyArmor);
            bool checkLegsArmor = Equipment.TryGetValue(Slot.LegSlot, out Item legsArmor);

            
            if (checkHeadArmor)
            {
                Armor armor = (Armor)headArmor;
                armorBonus += new HeroAttributes()
                {
                    Strength = armor.Attributes.Strength,
                    Dexterity = armor.Attributes.Dexterity,
                    Intelligence = armor.Attributes.Intelligence
                };
            }

            if (checkBodyArmor)
            {
                Armor armor = (Armor)bodyArmor;
                armorBonus += new HeroAttributes() 
                {
                    Strength = armor.Attributes.Strength, 
                    Dexterity = armor.Attributes.Dexterity,
                    Intelligence = armor.Attributes.Intelligence
                };
            }

            if (checkLegsArmor)
            {
                Armor armor = (Armor)legsArmor;
                armorBonus += new HeroAttributes() 
                { 
                    Strength = armor.Attributes.Strength, 
                    Dexterity = armor.Attributes.Dexterity, 
                    Intelligence = armor.Attributes.Intelligence
                };
            }

            return BaseHeroAttributes + armorBonus;
             //AddingNewEventArgs armor attributes later
        }


    }
}
