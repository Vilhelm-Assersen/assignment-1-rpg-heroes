# RPG Heroes creation

## What does the program do?

This program makes you able to choose a class, level up and manage your equipment! Your classes are mage, ranger, rogue and warrior. Each class has the same three attributes strength, dexterity and intelligence. Each class has their own main attribute which increases their damage. By leveling up your attributes increase depending on your chosen class

## What's the point of this sort of program?

This type of RPG program is a great way to start learning about classes, inheritance and unit testing in greater detail. By completing this assignment I've gotten a better insight into how classes and objects work in C# as well as unit testing. 

## How Do I know anything works without using Main method?

Instead of using main to play this game we use the unit tests to see if it'd work had we played it. This let's us know that our methods work as intended. I had trouble with gitlab and generally strougled at some parts of this assignment. So I wasn't able to complete all the unit tests, but the general idea of how they work can still be seen.
